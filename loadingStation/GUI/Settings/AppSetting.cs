﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Windows.Forms;
using Transitions;

using loadingStation.Base.Connection.Devices.Smartdevice;
using loadingStation.Base.Configuration.Config;
using loadingStation.Base.Function;
using Core;

namespace loadingStation.GUI.Settings
{
    public partial class AppSetting : Form
    {
        #region DropShadow Properties
        private const int CS_DROPSHADOW = 0x00020000;
        protected override CreateParams CreateParams
        {
            get
            {
                // add the drop shadow flag for automatically drawing
                // a drop shadow around the form
                CreateParams cp = base.CreateParams;
                cp.ClassStyle |= CS_DROPSHADOW;
                return cp;
            }
        }
        #endregion

        bool ModeChanged = false;
        int index = 0;
        List<string> ListValue = new List<string>();
        System.Collections.IEnumerator enumerator = App.Default.Properties.GetEnumerator();

        public AppSetting()
        {
            InitializeComponent();
            Region = System.Drawing.Region.FromHrgn(Actions.CreateRoundRectRgn(0, 0, Width, Height, 25, 25));

            Actions.RecenterLocation(this);
            Helper.AnimateBounce(this, 550, Location.X, Location.Y + 15);

            while (enumerator.MoveNext())
            {
                listProperties.Items.Add((((System.Configuration.SettingsProperty)enumerator.Current).Name));
            }

            foreach (SettingsProperty settings in App.Default.Properties)
            {
                object value = App.Default[settings.Name];
                if (value != null)
                    ListValue.Add(value.ToString());
                else
                    ListValue.Add("NULL");
            }

            //txtValue.Text = ListValue[index].ToString();
        }

        #region Event
        private void BtnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BtnRestart_Click(object sender, EventArgs e)
        {
            ModeSaveSetting();

            lblDashboard.Text = "Restart Safely, Please Wait";

            Application.DoEvents();

            foreach (ModbusOutput sd in GlobalProperties.DevicesOutput.Values)
            {
                // Close All Valve
                sd.ResetAllBit();
            }

            System.Threading.Thread.Sleep(1000);

            foreach (ModbusInput sd in GlobalProperties.DevicesInput.Values)
            {
                sd.StopLogging();
            }

            System.Threading.Thread.Sleep(1000);
            Actions.RelaunchApplication();
        }

        private void BtnExit_Click(object sender, EventArgs e)
        {
            ModeSaveSetting();

            // STOP LOGGING
            GlobalProperties.FLAG_LOGGING = false;

            lblDashboard.BackColor = System.Drawing.Color.FromArgb(235, 77, 75);
            lblDashboard.Text = "Exit Safely, Please Wait";

            Application.DoEvents();

            foreach (ModbusOutput sd in GlobalProperties.DevicesOutput.Values)
            {
                // Close All Valve
                sd.ResetAllBit();
            }

            System.Threading.Thread.Sleep(1100);

            foreach (ModbusInput sd in GlobalProperties.DevicesInput.Values)
            {
                sd.StopLogging();
            }
            
            System.Threading.Thread.Sleep(1000);
            Environment.Exit(0);
        }
        private void ListProperties_SelectedIndexChanged(object sender, EventArgs e)
        {
            index = listProperties.SelectedIndex;
            txtValue.Text = ListValue[index].ToString();
        }
        #endregion

        private void AppSetting_Load(object sender, EventArgs e)
        {
            bool ModeIndicator = Base.Configuration.Config.App.Default.IndicatorTestOnly;
            bool ModeDebug = Base.Configuration.Config.App.Default.IsDebugging;

            rbIndicator.Checked = (ModeIndicator) ? true : false;
            rbDebug.Checked = (ModeDebug) ? true : false;
            rbRelease.Checked = (!ModeDebug && !ModeIndicator) ? true : false;
        }

        private void RbRelease_CheckedChanged(object sender, EventArgs e)
        {
            ModeChanged = true;
        }

        private void RbDebug_CheckedChanged(object sender, EventArgs e)
        {
            ModeChanged = true;
        }

        private void RbIndicator_CheckedChanged(object sender, EventArgs e)
        {
            ModeChanged = true;
        }

        private void ModeSaveSetting()
        {
            if (ModeChanged)
            {
                bool Release = rbRelease.Checked;
                bool Debug = rbDebug.Checked;
                bool Indicator = rbIndicator.Checked;

                Base.Configuration.Config.App.Default.IsDebugging = (Release) ? false : true;
                Base.Configuration.Config.App.Default.IsDebugging = (Debug) ? true : false;
                Base.Configuration.Config.App.Default.IndicatorTestOnly = (Indicator) ? true : false;

                Base.Configuration.Jsonconfig.GenerateConfig();
            }
        }

        private void BtnSecurity_Click(object sender, EventArgs e)
        {
            using(SecuritySetting n = new SecuritySetting())
            {
                n.ShowDialog();
            }
        }
    }
}
