﻿using loadingStation.Base.Connection.Devices.Smartdevice;
using loadingStation.Base.Function;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

namespace loadingStation.Base.Connection.Socket
{
    public static class Server
    {
        private static ModbusAichi DevicePulse;

        public static void StartServer()
        {
            if (GlobalProperties.CoolantType is 'A')
            {
                AddCommand();

                Core.Connection.SocketServerSingle.Hostname = "127.0.0.1";
                Core.Connection.SocketServerSingle.Port = 9090;
                Core.Connection.SocketServerSingle.StartServer();
                Debug.WriteLine("Server Started");
            }
        }

        private static void AddCommand()
        {
            var Dict = Core.Connection.SocketServerSingle.DictCommandList;

            Dict.Add("AICHI_RESET_A", new AichiResetA());
            Dict.Add("AICHI_RESET_B", new AichiResetB());
            Dict.Add("AICHI_RESET_C", new AichiResetC());

            Dict.Add("AICHI_VALUE_A", new AichiValueA());
            Dict.Add("AICHI_VALUE_B", new AichiValueB());
            Dict.Add("AICHI_VALUE_C", new AichiValueC());
        }

        #region RESET
        private class AichiResetA : Core.Connection.SocketServerCommand
        {
            public override object Value()
            {
                if(GlobalProperties.ModbusPulse != null)
                    GlobalProperties.ModbusPulse.SetReset('A');
                return "RESET";
            }
        }

        private class AichiResetB : Core.Connection.SocketServerCommand
        {
            public override object Value()
            {
                if (GlobalProperties.ModbusPulse != null)
                    GlobalProperties.ModbusPulse.SetReset('B');
                return "RESET";
            }
        }

        private class AichiResetC : Core.Connection.SocketServerCommand
        {
            public override object Value()
            {
                if (GlobalProperties.ModbusPulse != null)
                    GlobalProperties.ModbusPulse.SetReset('C');
                return "RESET";
            }
        }
        #endregion

        #region VALUES

        private class AichiValueA : Core.Connection.SocketServerCommand
        {
            public override object Value()
            {
                if (GlobalProperties.ModbusPulse != null)
                {
                    var Device = GlobalProperties.ModbusPulse;
                    int CoolantHI, CoolantLO;
                    int WaterHI, WaterLO;
                    string result;

                    Device.GetData("CHA3", out CoolantHI);
                    Device.GetData("CHA4", out CoolantLO);

                    Device.GetData("CHA5", out WaterHI);
                    Device.GetData("CHA6", out WaterLO);

                    result = $"{CoolantHI},{CoolantLO},{WaterHI},{WaterLO}";
                    return result;
                }
                else
                {
                    return "NULL";
                }
            }
        }

        private class AichiValueB : Core.Connection.SocketServerCommand
        {
            public override object Value()
            {
                if (DevicePulse != null)
                {
                    var Device = DevicePulse;
                    int CoolantHI, CoolantLO;
                    int WaterHI, WaterLO;
                    string result;

                    Device.GetData("CHA7", out CoolantHI);
                    Device.GetData("CHA8", out CoolantLO);

                    Device.GetData("CHA9", out WaterHI);
                    Device.GetData("CHA10", out WaterLO);

                    result = $"{CoolantHI},{CoolantLO},{WaterHI},{WaterLO}";
                    return result;
                }
                else
                {
                    return "NULL";
                }
                
            }
        }

        private class AichiValueC : Core.Connection.SocketServerCommand
        {
            public override object Value()
            {
                if (DevicePulse != null)
                {
                    var Device = DevicePulse;
                    int CoolantHI, CoolantLO;
                    int WaterHI, WaterLO;
                    string result;

                    Device.GetData("CHA11", out CoolantHI);
                    Device.GetData("CHA12", out CoolantLO);

                    Device.GetData("CHA13", out WaterHI);
                    Device.GetData("CHA14", out WaterLO);

                    result = $"{CoolantHI},{CoolantLO},{WaterHI},{WaterLO}";
                    return result;
                }
                else
                {
                    return "NULL";
                }
                
            }
        } 
        #endregion
    }
}
