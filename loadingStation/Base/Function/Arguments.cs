﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.Diagnostics;

using loadingStation.Base.Configuration.Config;

namespace loadingStation.Base.Function
{
    class Arguments
    {
        [DllImport("kernel32.dll")]
        static extern bool AttachConsole(int dwProcessId);
        private const int ATTACH_PARENT_PROCESS = -1;

        public static List<string> Args = new List<string>();
        static int map = 0;
        public static void Execute()
        {
            foreach (string commandArgs in Args)
            {
                ExecuteCommand(commandArgs, map);
                map += 1;
            }
        }

        public static void ExecuteCommand(string commandArgs, int locateArgs)
        {
            AttachConsole(ATTACH_PARENT_PROCESS);
            string data = "";

            try
            {
                data = Args[locateArgs + 1].ToString();
            }
            catch
            {
                data = "";
            }

            try
            {
                switch (commandArgs)
                {
                    case "-host":
                        Databaseconfig.Default.Host = data;
                        Databaseconfig.Default.Save();
                        Databaseconfig.Default.Upgrade();
                        Console.WriteLine("\nHost Applied " + data);
                        break;

                    case "-db":
                        Databaseconfig.Default.DB = data;
                        Databaseconfig.Default.Save();
                        Databaseconfig.Default.Upgrade();
                        Console.WriteLine("\nDB Applied " + data);
                        break;

                    case "-username":
                        Databaseconfig.Default.Username = data;
                        Databaseconfig.Default.Save();
                        Databaseconfig.Default.Upgrade();
                        Console.WriteLine("\nUsername Applied " + data);
                        break;

                    case "-password":
                        Databaseconfig.Default.Password = data;
                        Databaseconfig.Default.Save();
                        Databaseconfig.Default.Upgrade();
                        Console.WriteLine("\nPassword Applied " + data);
                        break;

                    case "$PWDNULL":
                        Databaseconfig.Default.Password = string.Empty;
                        Databaseconfig.Default.Save();
                        Databaseconfig.Default.Upgrade();
                        Console.WriteLine("\nPassword NULLED");
                        break;

                    case "-modbusinputip":
                        Modbusconfig.Default.ModbusDebugInput = data;
                        Modbusconfig.Default.Save();
                        Modbusconfig.Default.Upgrade();
                        Console.WriteLine("\nModbus Input Applied " + data);
                        break;

                    case "-modbusoutputip":
                        Modbusconfig.Default.ModbusDebugOutput = data;
                        Modbusconfig.Default.Save();
                        Modbusconfig.Default.Upgrade();
                        Console.WriteLine("\nModbus Output Applied " + data);
                        break;

                    case "-debug":
                        Debugmode();
                        App.Default.IsDebugging = true;
                        App.Default.Save();
                        App.Default.Upgrade();
                        Console.WriteLine("\nDebug Mode Applied ");
                        break;

                    case "-release":
                        Releasemode();
                        App.Default.IsDebugging = false;
                        App.Default.Save();
                        App.Default.Upgrade();
                        Console.WriteLine("\nRelease Mode Applied");
                        break;

                    case "-adminpass":
                        App.Default.AdministratorPassword = data;
                        App.Default.Save();
                        App.Default.Upgrade();
                        Console.WriteLine("\nAdminPassword Applied " + data);
                        break;

                    case "-resetdefault":
                        ResetDefault();
                        Console.WriteLine("\n-- RESET Done " + data);
                        break;

                    case "-getconfiguration":
                        GetConfiguration();
                        break;

                    case "-errorlog":
                        Logs.Default.IsLogEnabled = (data == "true") ? true : false;
                        Console.WriteLine("\nLog Applied " + data);
                        break;

                }
            }
            catch (Exception x)
            {
                Debug.WriteLine(x);
            }
        }

        #region Custom Command
        private static void ResetDefault()
        {
            // Default Value (DB)
            string defaultHost = Databaseconfig.Default.default_Host;
            string defaultDB = Databaseconfig.Default.default_DB;
            string defaultUsername = Databaseconfig.Default.default_Username;
            string defaultPassword = Databaseconfig.Default.default_Password;

            // Default Value (Application)
            string defaultadminpass = App.Default.default_AdministratorPassword;
            bool defaultisdebugging = App.Default.default_IsDebugging;

            // Write From Default Value

            // --DB
            Databaseconfig.Default.Host = defaultHost;
            Databaseconfig.Default.DB = defaultDB;
            Databaseconfig.Default.Username = defaultUsername;
            Databaseconfig.Default.Password = defaultPassword;

            // --Application
            App.Default.IsDebugging = defaultisdebugging;
            App.Default.AdministratorPassword = defaultadminpass;

            // Save Configuration
            Databaseconfig.Default.Save();
            App.Default.Save();
        }

        private static void Debugmode()
        {
            // Default Value DB
            string username = "root";
            string password = "root";
            string db = "sfdb";
            string host = "127.0.0.1";

            // Write Value
            Databaseconfig.Default.Host = host;
            Databaseconfig.Default.DB = db;
            Databaseconfig.Default.Username = username;
            Databaseconfig.Default.Password = password;

            // Save Configuration
            Databaseconfig.Default.Save();

            Console.WriteLine("\nDatabase Config Set to Debug Mode");
        }

        private static void Releasemode()
        {
            // Default Value DB
            string username = "root";
            string password = "root";
            string db = "sfdb";
            string host = "192.168.11.254";

            // Write Value
            Databaseconfig.Default.Host = host;
            Databaseconfig.Default.DB = db;
            Databaseconfig.Default.Username = username;
            Databaseconfig.Default.Password = password;

            // Save Configuration
            Databaseconfig.Default.Save();

            Console.WriteLine("\nDatabase Config Set to Release Mode");
        }

        private static void GetConfiguration()
        {
            string DeviceFrom = null;
            DeviceFrom = (App.Default.IsDebugging) ? "Localhost" : "Database";

            Console.WriteLine("\n\n\n== Database Configuration ==");
            Console.WriteLine("Hostname\t: " + Databaseconfig.Default.Host);
            Console.WriteLine("Database\t: " + Databaseconfig.Default.DB);
            Console.WriteLine("Username\t: " + Databaseconfig.Default.Username);
            Console.WriteLine("Password\t: " + Databaseconfig.Default.Password);

            Console.WriteLine("");

            Console.WriteLine("== Application Configuration ==");
            Console.WriteLine("IsDebugging\t: " + App.Default.IsDebugging);
            Console.WriteLine("AppVersion\t: " + Application.ProductVersion.ToString());

            Console.WriteLine("");

            Console.WriteLine("== Error Log Configuration ==");
            Console.WriteLine("AllowSaveLog\t: " + Logs.Default.IsLogEnabled);
            Console.WriteLine("LastCollectLog\t: " + Logs.Default.LastlogTime);

            Console.WriteLine("");

            Console.WriteLine("== Modbus Configuration ==");
            Console.WriteLine("Load Device\t: " + DeviceFrom);
            Console.WriteLine("ModbusInput(debug)\t: " + Modbusconfig.Default.ModbusDebugInput);
            Console.WriteLine("ModbusOutput(debug)\t: " + Modbusconfig.Default.ModbusDebugOutput);

            Console.WriteLine("\n");
        }
        #endregion
    }
}
