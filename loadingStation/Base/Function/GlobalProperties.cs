﻿using System.Collections.Generic;
using loadingStation.Base.Connection.Devices.Smartdevice;

namespace loadingStation.Base.Function
{
    class GlobalProperties
    {
        /// <summary>
        /// Modbus Devices
        /// </summary>
        public static Dictionary<string, ModbusInput> DevicesInput = new Dictionary<string, ModbusInput>();
        public static Dictionary<string, ModbusOutput> DevicesOutput = new Dictionary<string, ModbusOutput>();
        public static Dictionary<string, ModbusAichi> DevicesAichi = new Dictionary<string, ModbusAichi>();

        /// <summary>
        /// SET User ID
        /// </summary>
        public static string UserID { get; set; }

        /// <summary>
        /// Logging FLAG
        /// </summary>
        public static bool FLAG_LOGGING { get; set; } = false;
        public static bool FLAG_DEVICELOADED { get; set; } = false;

        /// <summary>
        /// SET Coolant Type (From Config.ini)
        /// </summary>
        public static char CoolantType { get; set; }

        /// <summary>
        /// SET Modbus IP & Status.
        /// </summary>
        /// 
        public static ModbusOutput ModbusOutput { get; set; }
        public static ModbusInput ModbusInput { get; set; }
        public static ModbusAichi ModbusPulse { get; set; }

        /// <summary>
        /// SET Database Status
        /// </summary>
        public static bool DatabaseStatus { get; set; } = false;

        /// <summary>
        /// Value : Mixing[0], Measure[1], Drum[2] (Converted)
        /// </summary>
        public static int[] Value = new int[3];

        /// <summary>
        /// Value : Mixing[0], Measure[1], Drum[2] (Real Value)
        /// </summary>
        public static int[] RealData = new int[3];

        /// <summary>
        /// Value : Feedback Valve1[0], Valve2[1]
        /// </summary>
        public static int[] FeedbackValve = new int[2];

        /// <summary>
        /// Valve Indicator
        /// </summary>
        public enum Valve
        {
            Open,
            Close
        }
        public enum Type
        {
            PumpDrum,
            PumpDist,
            ValveCoolant,
            ValveWater,
            Propeler,
            FlowMeter,
            ERR,
            Emergency
        }
        public static Valve PumpDrum = new Valve();
        public static Valve PumpDist = new Valve();
        public static Valve ValveCoolant = new Valve();
        public static Valve ValveWater = new Valve();
        public static Valve Propeler = new Valve();
        public static Valve FlowMeter = new Valve();

        /// <summary>
        /// Change Drum Notification
        /// </summary>
        public enum Drum
        {
            Alert,
            None
        }
        public static Drum ChangeDrumNotify = new Drum();

        /// <summary>
        /// SET Current Status : "Ready" "Filling" "Error"
        /// </summary>
        public enum Status
        {
            Ready,
            Filling,
            Error,
            Emergency
        };
        public static Status CurrentStatus = new Status();

        /// <summary>
        /// Timelapse Count Filling Duration
        /// </summary>
        public static string TimelapseValveWater;
        public static string TimelapseValveCoolant;
    }
}
