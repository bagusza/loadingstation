﻿using System;
using System.Diagnostics;
using Newtonsoft.Json;

using loadingStation.Base.Function;

namespace loadingStation.Base.Configuration
{
    class Conf
    {
        // Database
        public string Host { get; set; }
        public string Database { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        
        // Application
        public bool IsDebugging { get; set; }
        public bool IndicatorTestOnly { get; set; }
        public bool LoginUseAuthentication { get; set; }

        // Modbus
        public string ModbusInput { get; set; }
        public string ModbusOutput { get; set; }
    }
    public class Jsonconfig
    {
        public static void Loadconfig()
        {
            string json = Actions.FileToString("config.json");

            if (json != "")
            {
                Conf config = JsonConvert.DeserializeObject<Conf>(json);
                Config.Databaseconfig.Default.Host = config.Host;
                Config.Databaseconfig.Default.DB = config.Database;
                Config.Databaseconfig.Default.Username = config.Username;
                Config.Databaseconfig.Default.Password = config.Password;

                Config.App.Default.IsDebugging = config.IsDebugging;
                Config.App.Default.IndicatorTestOnly = config.IndicatorTestOnly;
                Config.App.Default.LoginUseAuthentication = config.LoginUseAuthentication;

                Config.Modbusconfig.Default.ModbusDebugInput = config.ModbusInput;
                Config.Modbusconfig.Default.ModbusDebugOutput = config.ModbusOutput;

                Config.Databaseconfig.Default.Save();
                Config.Databaseconfig.Default.Upgrade();
                Config.App.Default.Save();
                Config.App.Default.Upgrade();
                Config.Modbusconfig.Default.Save();
                Config.Modbusconfig.Default.Upgrade();
                Debug.WriteLine("Loaded Json Config");
            }
            else
            {
                GenerateConfig();
                Debug.WriteLine("Json Config Not Loaded, Autogenerate");
                Loadconfig();
            }
        }

        public static void GenerateConfig()
        {
            Conf config = new Conf
            {
                Host = Config.Databaseconfig.Default.Host,
                Database = Config.Databaseconfig.Default.DB,
                Username = Config.Databaseconfig.Default.Username,
                Password = Config.Databaseconfig.Default.Password,

                IsDebugging = Config.App.Default.IsDebugging,
                IndicatorTestOnly = Config.App.Default.IndicatorTestOnly,
                LoginUseAuthentication = Config.App.Default.LoginUseAuthentication,
                ModbusInput = Config.Modbusconfig.Default.ModbusDebugInput,
                ModbusOutput = Config.Modbusconfig.Default.ModbusDebugOutput
            };
            string json = JsonConvert.SerializeObject(config, Formatting.Indented);
            Actions.StringToFile("config.json", json);
            Debug.WriteLine("Write config.json OK");
        }
    }
}
