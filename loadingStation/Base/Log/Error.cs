﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Diagnostics;

using loadingStation.Base.Function;

namespace loadingStation.Base.Log
{
    class Error
    {

        #region Private Properties
        private static string _CollectionErrorString = "";

        private static void Add(string ErrorStr)
        {
            var datenow = DateTime.Now;
            var epochnow = Conversion.ConvertToUnixTimestamp(datenow);
            _CollectionErrorString +=
                string.Format
                (
                    "\n [{0}] - [{1}] \n" +
                    "Error: {2}"
                    , datenow.ToString()
                    , epochnow
                    , ErrorStr
                );
        }
        #endregion

        public static void Collect(string ErrorStr)
        {
            Add(ErrorStr);
        }

        public static void SaveLog()
        {
            try
            {
                Configuration.Logs.SetLastlog();

                string filepath = "Log";
                var check = Actions.CheckDirectory("Log");
                var datenow = DateTime.Now.ToString("dd_MMMM_yyyy_HH_mm_ss");
                var filename = string.Format(@"Log\Log_{0}.txt", datenow);
                var IsLogEnabled = Configuration.Logs.IsLogEnabled;

                if (IsLogEnabled)
                {
                    if (!(_CollectionErrorString == ""))
                    {
                        Actions.StringToFile(filename, _CollectionErrorString);
                    }
                }

                Actions.RemoveOlderFile(filepath);
                _CollectionErrorString = "";
            }
            catch { }
        }
    }
}
