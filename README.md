#  Loading Station
The filling and mixing system to calculate liquid coolant and water with own algorithm and based on PH measurement, after that distribute mixed liquid to fanuc robodrill system.

**note: some feature been removed cause confidential purpose.**






## Features

- Auto measurement liquid coolant and water mixing
- Use IOT device like (shj device, aichi flow, festo) for sensors connection
- User authentication with RFID, one tap staff ID Card
- Remote connection for maintenance by tcp socket
- Handle multiple action emergency

## Stack
- .NET Framework 4
- C# Winform
- Modbus
- MySQL


## Screenshot
![Dashboard](https://gitlab.com/bagusza/loadingstation/-/raw/main/screenshot.png){width=50%}
![Production](https://gitlab.com/bagusza/loadingstation/-/raw/main/production.jpeg){width=50%}


## Reference

 - [Festo Flow Sensor SFAW](https://www.festo.com/gb/en/p/flow-sensor-id_SFAW/?q=%7E%3AfestoSortOrderScored)
 - [Fanuc Robodrill](hhttps://www.fanuc.co.jp/en/product/robodrill/index.html)
 - [SHJ Electonic Modbus](hhttp://www.shjelectronic.com/)

